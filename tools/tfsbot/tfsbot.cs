using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

using Meebey.SmartIrc4net;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.VersionControl.Common;
using OpenTF.Common;

partial class NotifierDaemon
{
	private IrcClient irc = new	IrcClient();
	private int port = 6667;

	private static void Main(string[] arguments)
	{
		NotifierDaemon nd = new NotifierDaemon();
		nd.Run();
	}

	private void UpdateLatestChangeset(int cid)
	{
		// Open App.Config of executable
		System.Configuration.Configuration config =
			ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

		KeyValueConfigurationCollection settings = config.AppSettings.Settings;
		settings.Remove("tfs.changeset");
		settings.Add("tfs.changeset", cid.ToString());

		// Save the configuration file.
		config.Save(ConfigurationSaveMode.Modified);
	}

	public void CommandMessage(string s)
	{
		irc.SendMessage(SendType.Message, AppSettings.CommandChannel, s);
	}

	public void NotifyMessage(string s)
	{
		irc.SendMessage(SendType.Message, AppSettings.NotifyChannel, s);
	}
		
	public void Run()
	{
		if (AppSettings.LatestChangesetId == 0)
			UpdateLatestChangeset(VersionControlServer.GetLatestChangesetId() - 1);
	
		// Force a reload of a changed section.
		ConfigurationManager.RefreshSection("appSettings");
		irc.OnConnected += new EventHandler(OnConnected);
		irc.OnChannelMessage += new IrcEventHandler(OnChannelMessage);

		try
			{
				irc.Connect(AppSettings.IrcServer, port);
				irc.SendDelay = 1000;
				new Thread(new ThreadStart(MonitorCheckins)).Start();
				irc.Listen();
			}
		catch (Exception e)
			{
				Console.Write("Failed to connect: "+ e.Message);
			}
	}

	private VersionControlServer VersionControlServer
	{
		get 
			{
				NetworkCredential creds = new NetworkCredential(AppSettings.Username, AppSettings.Password, AppSettings.Domain);
				TeamFoundationServer tfs = TeamFoundationServerFactory.GetServer(AppSettings.TfsUrl, creds);
				return tfs.GetService(typeof(VersionControlServer)) as VersionControlServer;
			}
	}

	static public string ChangeTypeToString(ChangeType change)
	{
		string ctype = "edit";

		if ((change & ChangeType.Add) == ChangeType.Add) ctype = "add";
		else if ((change & ChangeType.Delete) == ChangeType.Delete) ctype = "delete";
		else if ((change & ChangeType.Rename) == ChangeType.Rename) ctype = "rename";

		return ctype;
	}
		
	void OnConnected(object sender, EventArgs e)
	{
		irc.Login(AppSettings.Nick, "Team Foundation Notification Daemon");
		irc.RfcJoin(AppSettings.CommandChannel);
		irc.RfcJoin(AppSettings.NotifyChannel);
	}

	void OnChangesetCommand(IrcEventArgs e)
	{
		if (e.Data.MessageArray.Length < 2)
			{
				CommandMessage("Usage: changeset <id>");
				return;
			}

		int cid = Convert.ToInt32(e.Data.MessageArray[1]);
		try
			{
				Changeset changeset = VersionControlServer.GetChangeset(cid, true, false);

				CommandMessage("Changeset: " + changeset.ChangesetId);
				CommandMessage("User: " + changeset.Committer);
				CommandMessage("Date: " + changeset.CreationDate);

				if (!String.IsNullOrEmpty(changeset.Comment))
					{
						CommandMessage("Comment:");
						CommandMessage("	" + changeset.Comment);
					}

				CommandMessage("Items:");
				foreach (Change change in changeset.Changes)
					{
						CommandMessage("	" + ChangeTypeToString(change.ChangeType) + " " + change.Item.ServerItem);
					}
			}
		catch (Exception ex)
			{
				CommandMessage(ex.Message);
			}
	}

	void OnChannelMessage(object sender, IrcEventArgs e)
	{
		if (e.Data.MessageArray.Length == 0) return;

		if (e.Data.Channel == AppSettings.NotifyChannel)
			{
				NotifyMessage("Talk to me on " + AppSettings.CommandChannel);
				return;
			}

		switch (e.Data.MessageArray[0])
			{
			case "changeset":
				OnChangesetCommand(e);
				break;
			case "stats":
				OnStatsCommand(e);
				break;
			}
	}

	void MonitorCheckins()
	{
		int lastestChangesetId = AppSettings.LatestChangesetId;

		while (true)
			{
				ChangesetVersionSpec versionFrom = new ChangesetVersionSpec(lastestChangesetId.ToString());
				IEnumerable changeSets = VersionControlServer.QueryHistory(VersionControlPath.RootFolder, 
																																	 VersionSpec.Latest, 0, RecursionType.Full, 
																																	 null, versionFrom, null, 100, false, false, false);
		
				Stack<string> msgStack = new Stack<string>();

				foreach (Changeset changeSet in changeSets)
					{
						if (changeSet.ChangesetId > lastestChangesetId) 
							{
								lastestChangesetId = changeSet.ChangesetId;
						
								string msg0 = String.Format("Url: {0}/VersionControl/Changeset.aspx?artifactMoniker={1}&webView=true",
																						AppSettings.TfsUrl, changeSet.ChangesetId);
								msgStack.Push(msg0);

								if (!String.IsNullOrEmpty(changeSet.Comment))
									{
										string msg1 = String.Format("Comment: {0}", changeSet.Comment);
										msgStack.Push(msg1);
									}

								string msg2 = String.Format("Changeset: {0} by {1} on {2}", 
																						changeSet.ChangesetId,
																						changeSet.Committer, 
																						changeSet.CreationDate.ToString());
								msgStack.Push(msg2);
							}
					}

				bool fnd = msgStack.Count > 0;
				while (msgStack.Count > 0)
					{
						string msg = msgStack.Pop();
						NotifyMessage(msg);
					}

				if (fnd) UpdateLatestChangeset(lastestChangesetId);
				Thread.Sleep(1000*300);
			}
	}
}