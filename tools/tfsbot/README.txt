This folder contains the OpenTF Commit Notification Daemon "tfsbot".

It is not built by default. To build on unix machines, type "make" at a 
shell prompt. On Windows, use the project file.

Before running tfsbot, be sure to configure the settings in tfsbot.exe.config.

The bot will join both irc.channel.command and irc.channel.notify. If you'd like 
your notifications on the same channel as the command channel you can just
use the configuration setting irc.channel.

On irc.channel.command, the tfsbot currently accepts the following commands:

1) changeset <cid>
2) stats

Enjoy.
