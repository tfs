using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Discovery;
using System.Web.Services.Protocols;

using Meebey.SmartIrc4net;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.VersionControl.Common;
using OpenTF.Common;

[System.Web.Services.WebServiceBinding(Name="AdminSoap", Namespace="http://schemas.microsoft.com/TeamFoundation/2005/06/VersionControl/Admin/03")]
public class AdminStats : System.Web.Services.Protocols.SoapHttpClientProtocol {
		
	public AdminStats(string url, ICredentials credentials) 
		{
			this.Url = url + "/VersionControl/v1.0/administration.asmx";
			this.Credentials = credentials;
		}
		
	[System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://schemas.microsoft.com/TeamFoundation/2005/06/VersionControl/Admin/03/QueryRepositoryInformation", RequestNamespace="http://schemas.microsoft.com/TeamFoundation/2005/06/VersionControl/Admin/03", ResponseNamespace="http://schemas.microsoft.com/TeamFoundation/2005/06/VersionControl/Admin/03", ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped, Use=System.Web.Services.Description.SoapBindingUse.Literal)]
		public AdminRepositoryInfo QueryRepositoryInformation() 
		{
			object[] results = this.Invoke("QueryRepositoryInformation", new object[0]);
			return ((AdminRepositoryInfo)(results[0]));
		}
}

[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/TeamFoundation/2005/06/VersionControl/Admin/03")]
public class AdminRepositoryInfo {
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int UserCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int GroupCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int WorkspaceCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int ShelvesetCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int FileCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int FolderCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int MaxChangesetID;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int PendingChangeCount;
		
	[System.Xml.Serialization.XmlAttributeAttribute(Namespace="")]
	public int ShelvesetDeletedCount;
}

partial class NotifierDaemon
{
	void OnStatsCommand(IrcEventArgs e)
	{
		NetworkCredential creds = new NetworkCredential(AppSettings.Username, AppSettings.Password, AppSettings.Domain);
		AdminStats stats = new AdminStats(AppSettings.TfsUrl, creds);
		AdminRepositoryInfo info = stats.QueryRepositoryInformation();

		CommandMessage("Files:						" + info.FileCount);
		CommandMessage("Folders:					" + info.FolderCount);
		CommandMessage("Groups:					" + info.GroupCount);
		CommandMessage("Pending Changes: " + info.PendingChangeCount);
		CommandMessage("Shelvesets:			" + info.ShelvesetCount);
		CommandMessage("Users:						" + info.UserCount);
		CommandMessage("Workspaces:			" + info.WorkspaceCount);
	}

}